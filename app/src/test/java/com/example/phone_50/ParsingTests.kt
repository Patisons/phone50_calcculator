package com.example.phone_50

import org.junit.Assert.*
import org.junit.Test
import java.math.BigDecimal
import java.math.BigDecimal.ONE
import java.math.BigDecimal.ROUND_CEILING
import java.math.BigInteger
import kotlin.math.log

class ParsingTests {
    // operatora prioritātes noteikšana izteiksmē
    @Test
    fun findPrior(){
        val mStr = "1*2+3"
        val tList = parsStringToArray(mStr)
        val (index, prior) = actionPriorFind(tList)
        assertEquals(3, index)
    }
    @Test
    fun findPrior0(){
        val mStr = "10/5/1"
        val tList = parsStringToArray(mStr)
        val (index, prior) = actionPriorFind(tList)
        assertEquals(3, index)
    }
    // pilnā izteiksmes apstrāde
    @Test
    fun calcOfExpr() {
        val mStr = "(1 (+, ) 2)"
        val tList = calculationOfExpression(mStr)
        assertEquals("error", tList)
    }

    @Test
    fun calcOfExpr01() {
        val mStr = "2^3"
        val tList = calculationOfExpression(mStr)
        assertEquals("8.0", tList)
    }

    @Test
    fun calcOfExpr04() {
        val mStr = "24/4/3"
        val tList = calculationOfExpression(mStr)
        assertEquals("2.0", tList)
    }
    @Test
    fun calcOfExpr05() {
        val mStr = "4^3^2"
        val tList = calculationOfExpression(mStr)
        assertEquals("262144.0", tList)
    }

    @Test
    fun calcOfExpr06() {
        val mStr = "3*3^2"
        val tList = calculationOfExpression(mStr)
        assertEquals("27.0", tList)
    }

    @Test
    fun calcOfExpr02() {
        val mStr = "2+4^(2+1)^2+1"
        val tList = calculationOfExpression(mStr)
        assertEquals("262147.0", tList)
    }

    @Test
    fun calcOfExpr03() {
        val mStr = "2*3^2^2+2"
        val tList = calculationOfExpression(mStr)
        assertEquals("164.0", tList)
    }

    @Test
    fun calcOfExpr0() {
        val mStr = "(1 (+ ) 2)"
        val tList = calculationOfExpression(mStr)
        assertEquals("3.0", tList)
    }

    @Test
    fun calcOfExpr1() {
        val mStr = "5*(2+3)/5-(2.5*2)"
        val tList = calculationOfExpression(mStr)
        assertEquals("0.0", tList)
    }

    @Test
    fun calcOfExpr2() {
        val mStr = "pi"
        val tList = calculationOfExpression(mStr)
        assertEquals("3.141592653589793", tList)
    }

    @Test
    fun calcOfExpr21() {
        val mStr = "1+pi*2"
        val tList = calculationOfExpression(mStr)
        assertEquals("7.283185307179586", tList)
    }

    @Test
    fun calcOfExpr3() {
        val mStr = "round(2.5555,4)"
        val tList = calculationOfExpression(mStr)
        assertEquals("2.5555", tList)
    }

    @Test
    fun calcOfExpr31() {
        val mStr = "round(2.55555,4)"
        val tList = calculationOfExpression(mStr)
        assertEquals("2.5556", tList)
    }

    @Test
    fun calcOfExpr22() {
        val mStr = "round(1+pi*2,4)"
        val tList = calculationOfExpression(mStr)
        assertEquals("7.2832", tList)
    }

    // Izteiksmes rēķināšana
    @Test
    fun calc() {
        val mStr = "1"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        assertTrue(rez1.compareTo(ONE) == 0)
    }

    @Test
    fun calc0() {
        val mStr = "1+2"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("3")
        assertTrue(rez1.compareTo(arg) == 0)
    }

    @Test
    fun calc1() {
        val mStr = "1+2*3"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("7")
        assertTrue(rez1.compareTo(arg) == 0)
    }

    @Test
    fun calc11() {
        val mStr = "1*2+3"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("5")
        assertTrue(rez1.compareTo(arg) == 0)
    }

    @Test
    fun calc12() {
        val mStr = "1*2+3*2"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("8")
        assertTrue(rez1.compareTo(arg) == 0)
    }

    @Test
    fun calc3() {
        val mStr = "10.5*2+3-2*3"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("18")
        assertTrue(rez1.compareTo(arg) == 0)
    }

    @Test
    fun calcx() {
        val mStr = "(8)"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("8")
        assertTrue(rez1.compareTo(arg) == 0)
    }

    @Test
    fun calcx1() {
        val mStr = "1.2+3.4"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("4.6")
        assertTrue(rez1.compareTo(arg) == 0)
    }
    @Test
    fun calcx2() {
        val mStr = "2.5-1.2"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("1.3")
        assertTrue(rez1.compareTo(arg) == 0)
    }
    @Test
    fun calcx3() {
        val mStr = "1.3*2.5"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("3.25")
        assertTrue(rez1.compareTo(arg) == 0)
    }
    @Test
    fun calcx4() {
        val mStr = "5.6/1.2"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("4.6666666666666667")
        assertTrue(rez1.compareTo(arg) == 0)
    }

    @Test
    fun calc2() {
        val mStr = "sin(1.5707963267948966)"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("1")
        assertTrue(rez1.compareTo(arg) == 0)
    }

    @Test
    fun calc4() {
        val mStr = "5*(2+3)+12"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("37")
        assertTrue(rez1.compareTo(arg) == 0)
    }

    @Test
    fun calc5() {
        val mStr = "5*(2+3)/5-(2.5*2)"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal(BigInteger.ZERO)
        assertTrue(rez1.compareTo(arg) == 0)
    }

    @Test
    fun calc6() {
        val mStr = "log(10,2)"
        val rez = log(10.0, 2.0)
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("3.3219280948873626")
        assertTrue(rez1.compareTo(arg) == 0)
    }

    @Test
    fun calc7() {
        val mStr = "log(5+5,2)"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("3.3219280948873626")
        assertTrue(rez1.compareTo(arg) == 0)
    }

    @Test
    fun calc8() {
        val mStr = "5+(-2)+1"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("4")
        assertTrue(rez1.compareTo(arg) == 0)
    }

    @Test
    fun calc9() {
        val mStr = "123.2+1.2*(2)"
        val tList = parsStringToArray(mStr)
        val (rez1, rez2) = calculation(tList)
        val arg: BigDecimal = BigDecimal("125.6")
        assertTrue(rez1.compareTo(arg) == 0)
    }

    // atrod pirmās veicamās darbības (lūzuma punkta) indeksu
    @Test
    fun FindOperatorTests() {
        val mStr = "2+3"
        val mElList = parsStringToArray(mStr)
        assertEquals(1, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests01() {
        val mStr = "2*3/2"
        val mElList = parsStringToArray(mStr)
        assertEquals(1, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests02() {
        val mStr = "x*y/z"
        val mElList = parsStringToArray(mStr)
        assertEquals(1, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests0() {
        val mStr = "(x+10.2)^2+5*y-z"
        val mElList = parsStringToArray(mStr)
        assertEquals(7, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests00() {
        val mStr = "(x+10.2)^(2+5*y-z)"
        val mElList = parsStringToArray(mStr)
        assertEquals(5, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests1() {
        val mStr = "15+(2+3)+12"
        val mElList = parsStringToArray(mStr)
        assertEquals(1, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests11() {
        val mStr = "15*(2+3)/12+(1+2)"
        val mElList = parsStringToArray(mStr)
        assertEquals(9, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests12() {
        val mStr = "15*(2+3)/12*(1+2)+5"
        val mElList = parsStringToArray(mStr)
        assertEquals(15, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests2() {
        val mStr = "15/(2*(15+2)+3)+12"
        val mElList = parsStringToArray(mStr)
        assertEquals(13, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests3() {
        val mStr = "15+(2*(15+2)+3)*12"
        val mElList = parsStringToArray(mStr)
        assertEquals(1, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests4() {
        val mStr = "15*(2*(15+2)+3)+12"
        val mElList = parsStringToArray(mStr)
        assertEquals(13, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests5() {
        val mStr = "(2*(15+2)+3)+12"
        val mElList = parsStringToArray(mStr)
        assertEquals(11, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests6() {
        val mStr = "15*2/sin(x)+3+12"
        val mElList = parsStringToArray(mStr)
        assertEquals(8, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests7() {
        val mStr = "sin(x)+3+12"
        val mElList = parsStringToArray(mStr)
        assertEquals(4, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests8() {
        val mStr = "sin(x)*3+12"
        val mElList = parsStringToArray(mStr)
        assertEquals(6, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests9() {
        val mStr = "sin(x)"
        val mElList = parsStringToArray(mStr)
        assertEquals(0, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests10() {
        val mStr = "10/2-3*1"
        val mElList = parsStringToArray(mStr)
        assertEquals(3, findCurrentOperator(mElList))
    }

    @Test
    fun FindOperatorTests13() {
        val mStr = "20,5"
        val mElList = parsStringToArray(mStr)
        assertEquals(1, findCurrentOperator(mElList))
    }

    // operatoru prioritātes noteikšana
    @Test
    fun OperatorPriorTests10() {
        val mStr = "fun"
        assertEquals(1, parsOperPriority(mStr))
    }

    @Test
    fun OperatorPriorTests11() {
        val mStr = "*"
        assertEquals(4, parsOperPriority(mStr))
    }

    @Test
    fun OperatorPriorTests12() {
        val mStr = "xx"
        assertEquals(0, parsOperPriority(mStr))
    }

    // pārvērš elementu masīvu String rindā
    @Test
    fun createParsString_3() {
        val mString = "1+12.5*8.1"
        val tList = arrayListOf("1", "+", "12.5", "*", "8.1")
        assertEquals(mString, parsArrayToString(tList, 0, tList.size - 1))
    }

    @Test
    fun createParsString_1() {
        val mString = "1+12.5*8.1"
        val tList = arrayListOf("1", "+", "12.5", "*", "8.1")
        assertEquals(mString, parsArrayToString(tList, 0))
    }

    @Test
    fun createParsString_2() {
        val mString = "12.5*8.1"
        val tList = arrayListOf("1", "+", "12.5", "*", "8.1")
        assertEquals(mString, parsArrayToString(tList, 2, 5))
    }

    @Test
    fun createParsString_3_1() {
        val mString = "8.1"
        val tList = arrayListOf("8.1")
        assertEquals(mString, parsArrayToString(tList, 0, 2))
    }

    @Test
    fun createParsString_4() {
        val mString = "(12.1)"
        val tList = arrayListOf("(", "1", "+", "202", ")", "*", "(", "12.1", ")")
        assertEquals(mString, parsArrayToString(tList, 6, tList.size))
    }

    @Test
    fun createParsString_5() {
        val mString = "(12,5)"
        val tList = arrayListOf("log", "(", "12", ",", "5", ")")
        assertEquals(mString, parsArrayToString(tList, 1, tList.size))
    }

    @Test
    fun createParsString_6() {
        val mString = "log(12,5+2)"
        val tList = arrayListOf("log", "(", "12", ",", "5", "+", "2", ")")
        assertEquals(mString, parsArrayToString(tList, 0, tList.size))
    }

    @Test
    fun createParsString_7() {
        val mString = "log(12,5+(-2))"
        val tList = arrayListOf("log", "(", "12", ",", "5", "+", "(", "-2", ")", ")")
        assertEquals(mString, parsArrayToString(tList, 0, tList.size))
    }

    // atrod noteikt dziļuma iekavas no kreisās puses
    @Test
    fun bracketFindTests() {
        val mStr = "2+3"
        val fLev = 1
        val mRez = arrayOf(-1, -1, 0)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsFindLevel(mElList, fLev))
    }

    @Test
    fun bracketFindTests1() {
        val mStr = "(2+3)"
        val fLev = 1
        val mRez = arrayOf(-1, -1, 0)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsFindLevel(mElList, fLev))
    }

    @Test
    fun bracketFindTests2() {
        val mStr = "15+(2+3)+12"
        val fLev = 1
        val mRez = arrayOf(2, 6, fLev)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsFindLevel(mElList, fLev))
    }

    @Test
    fun bracketFindTests3() {
        val mStr = "15+(2*(15+2)+3)+12"
        val fLev = 2
        val mRez = arrayOf(5, 9, fLev)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsFindLevel(mElList, fLev))
    }

    @Test
    fun bracketFindTests4() {
        val mStr = "15+(2*(15+2+sin(x))+3)+12"
        val fLev = 2
        val mRez = arrayOf(5, 14, fLev)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsFindLevel(mElList, fLev))
    }

    @Test
    fun bracketFindTests5() {
        val mStr = "15+(2*(1+(5+(6-2))))"
        val fLev = 3
        val mRez = arrayOf(8, 16, fLev)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsFindLevel(mElList, fLev))
    }

    @Test
    fun bracketFindTests6() {
        val mStr = "15+(2*(5+(6-2))-(15+2)+1)"
        val fLev = 1
        val mRez = arrayOf(2, 22, fLev)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsFindLevel(mElList, fLev))
    }

    // atrod pašas dziļākās iekavas
    @Test
    fun bracketTests() {
        val mStr = "2+3"
        val mRez = arrayOf(-1, -1, 0)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsDownLevel(mElList))
    }

    @Test
    fun bracketTests1() {
        val mStr = "(2+3)"
        val mRez = arrayOf(-1, -1, 0)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsDownLevel(mElList))
    }

    @Test
    fun bracketTests2() {
        val mStr = "15+(2+3)+12"
        val mRez = arrayOf(2, 6, 1)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsDownLevel(mElList))
    }

    @Test
    fun bracketTests3() {
        val mStr = "15+(2*(15+2)+3)+12"
        val mRez = arrayOf(5, 9, 2)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsDownLevel(mElList))
    }

    @Test
    fun bracketTests4() {
        val mStr = "15+(2*(15+2+sin(x))+3)+12"
        val mRez = arrayOf(11, 13, 3)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsDownLevel(mElList))
    }

    @Test
    fun bracketTests5() {
        val mStr = "15+(2*(15+2)+1+(5+(6-2)))"
        val mRez = arrayOf(16, 20, 3)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsDownLevel(mElList))
    }

    @Test
    fun bracketTests6() {
        val mStr = "15+(2*(5+(6-2))-(15+2)+1)"
        val mRez = arrayOf(8, 12, 3)
        val mElList = parsStringToArray(mStr)
        assertArrayEquals(mRez, bracketsDownLevel(mElList))
    }

    // parsings elementu masīva izveide
    @Test
    fun createParslist_() {
        val mString = "1+2"
        val tList = arrayListOf("1", "+", "2")
        assertEquals(tList, parsStringToArray(mString))
    }

    @Test
    fun createParslist_1() {
        val mString = "1"
        val tList = arrayListOf("1")
        assertEquals(tList, parsStringToArray(mString))
    }

    @Test
    fun createParslist_2() {
        val mString = "+"
        val tList = arrayListOf("+")
        assertEquals(tList, parsStringToArray(mString))
    }

    @Test
    fun createParslist_3() {
        val mString = "1+12.5*8.1"
        val tList = arrayListOf("1", "+", "12.5", "*", "8.1")
        assertEquals(tList, parsStringToArray(mString))
    }

    @Test
    fun createParslist_3_1() {
        val mString = "8.1"
        val tList = arrayListOf("8.1")
        assertEquals(tList, parsStringToArray(mString))
    }

    @Test
    fun createParslist_4() {
        val mString = "(1+202)*(12.1)"
        val tList = arrayListOf("(", "1", "+", "202", ")", "*", "(", "12.1", ")")
        assertEquals(tList, parsStringToArray(mString))
    }

    @Test
    fun createParslist_5() {
        val mString = "sin(x)+2"
        val tList = arrayListOf("sin", "(", "x", ")", "+", "2")
        assertEquals(tList, parsStringToArray(mString))
    }

    @Test
    fun createParslist_6() {
        val mString = "((1+202)*(12.1))"
        val tList = arrayListOf("(", "1", "+", "202", ")", "*", "(", "12.1", ")")
        assertEquals(tList, parsStringToArray(mString))
    }

    @Test
    fun createParslist_7() {
        val mString = "((((1+202)*(12.1))))"
        val tList = arrayListOf("(", "1", "+", "202", ")", "*", "(", "12.1", ")")
        assertEquals(tList, parsStringToArray(mString))
    }

    @Test
    fun createParslist_8() {
        val mString = "log(12,5)"
        val tList = arrayListOf("log", "(", "12", ",", "5", ")")
        assertEquals(tList, parsStringToArray(mString))
    }

    @Test
    fun createParslist_9() {
        val mString = "log(12,5+2)"
        val tList = arrayListOf("log", "(", "12", ",", "5", "+", "2", ")")
        assertEquals(tList, parsStringToArray(mString))
    }

    @Test
    fun createParslist_10() {
        val mString = "log(12,5+(-2))"
        val tList = arrayListOf("log", "(", "12", ",", "5", "+", "(", "-2", ")", ")")
        assertEquals(tList, parsStringToArray(mString))
    }

    // Parsinga leksika  vai pareiza
    @Test
    fun pLex_() {
        assertEquals(false, parsLeks("(1+2"))
    }

    @Test
    fun pLex_0() {
        assertEquals(false, parsLeks("(1+2))"))
    }

    @Test
    fun pLex_1() {
        assertEquals(false, parsLeks("(1(+)2"))
    }

    @Test
    fun pLex_2() {
        assertEquals(true, parsLeks("(1+2)"))
    }

    @Test
    fun pLex_3() {
        assertEquals(true, parsLeks("2+(1+2)"))
    }

    @Test
    fun pLex_4() {
        assertEquals(true, parsLeks("(1+2)+2"))
    }

    // List copy test
    @Test
    fun copy() {
        val fList = arrayListOf("1", "2", "3", "4")
        val sList = arrayListOf("1", "2", "3", "4")
        assertEquals(sList, copyArrayList(fList, 0))
    }

    @Test
    fun copy1() {
        val fList = arrayListOf("1", "2", "3", "4")
        val sList = arrayListOf("3", "4")
        assertEquals(sList, copyArrayList(fList, 2))
    }

    @Test
    fun copy2() {
        val fList = arrayListOf("0", "1", "2", "3", "4", "5", "6")
        val sList = arrayListOf("2", "3", "4")
        assertEquals(sList, copyArrayList(fList, 2, 4))
    }
}