package com.example.phone_50

import org.junit.Test
import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    // vēstures saglabāšanas testi
    @Test
    fun test() {
        val numbers = intArrayOf(2, 3, 4)
        val sum = sumNumbers(*numbers)
        println(sum) // Prints '9'
    }

    // Pēdējais ekrāna saturs
    @Test
    fun cHystory1() {
        var myHist = cHystory("1234")
        assertEquals("1234", myHist.toString())
    }

    @Test
    fun cHystory2() {
        var myHist = cHystory("1234")
        myHist = cHystory("9876")
        assertEquals("9876", myHist.toString())
    }

    // Atgriež formulu bez mainīgo rindas sākumā
    @Test
    fun clearingFormula() {
        assertEquals("a", getClearFormula("a@a"))
    }

    @Test
    fun clearingFormula1() {
        assertEquals("a+b", getClearFormula("ab@a+b"))
    }

    @Test
    fun clearingFormula2() {
        assertEquals("x+sin(y)", getClearFormula("xy@x+sin(y)"))
    }

    // atgriež saliktā Tag f-jas numuru, ja tags satur f-jas taustiņa tagu
    @Test
    fun retFunNumber() {
        assertEquals(-1, returnFunctionButtonNumber("inpTxtView vieTxtView"))
    }

    @Test
    fun retFunNumber1() {
        assertEquals(2, returnFunctionButtonNumber("inpTxtView btnFunc3"))
    }

    @Test
    fun retFunNumber2() {
        assertEquals(4, returnFunctionButtonNumber("btnFunc5 inpTxtView"))
    }

    @Test
    fun retFunNumber3() {
        assertEquals(-1, returnFunctionButtonNumber("inpTxtViewvieTxtView"))
    }

    // Atgriež vai pogas ir saistītas savā starpā
    @Test
    fun checkLink() {
        assertEquals(false, linkedButtonEquel("btnFunc1", "btnDig6"))
    }

    @Test
    fun checkLink1() {
        assertEquals(false, linkedButtonEquel("inpTxtView", "btnDig6"))
    }

    @Test
    fun checkLink2() {
        assertEquals(true, linkedButtonEquel("inpTxtView", "btnFunc3"))
    }

    @Test
    fun checkLink3() {
        assertEquals(true, linkedButtonEquel("btnFunc2", "inpTxtView"))
    }

    @Test
    fun checkLink4() {
        assertEquals(true, linkedButtonEquel("btnFunc2", "btnBack"))
    }

    // Atgriež vai poga ir saistīta ar citām
    @Test
    fun grupaSk_1() {
        createButtonList()
        assertEquals(true, checkLinkedButtonCount("inpTxtView"))
    }

    @Test
    fun grupaSk_2() {
        createButtonList()
        assertEquals(true, checkLinkedButtonCount("btnFunc2"))
    }

    @Test
    fun grupaSk_3() {
        createButtonList()
        assertEquals(false, checkLinkedButtonCount("xxx"))
    }

    // Atgriež saistīto pogu sarakstu List<String>
    @Test
    fun grupaList_1() {
        createButtonList()
        val tmp = listOf("inpTxtView")
        assertEquals(tmp, checkLinkedButtonList("vieTxtView"))
    }

    @Test
    fun grupaList_2() {
        createButtonList()
        val tmp = listOf("inpTxtView", "btnBack")
        assertEquals(tmp, checkLinkedButtonList("btnFunc1"))
    }

    @Test
    fun grupaList_21() {
        createButtonList()
        val tmp = listOf("vieTxtView", "btnFunc1", "btnFunc2", "btnFunc3", "btnFunc4", "btnFunc5")
        assertEquals(tmp, checkLinkedButtonList("inpTxtView"))
    }

    @Test
    fun grupaList_3() {
        createButtonList()
        val tmp = listOf<String>()
        assertEquals(tmp, checkLinkedButtonList("xxx"))
    }

    // taustiņu otrā nozīmepie vilkšanas
    @Test
    fun mySecKey1() {
        assertEquals("", findSecondSymbol("xxx"))
    }

    @Test
    fun mySecKey2() {
        assertEquals("y", findSecondSymbol("btnDig5"))
    }

    // saglabājamās formulas veidošana
    @Test
    fun formulString() {
        val tmp = "1+2"
        assertEquals("@".plus(tmp), createTenableFunctionString(tmp))
    }

    @Test
    fun formulString01() {
        val tmp = "a"
        assertEquals("a@".plus(tmp), createTenableFunctionString(tmp))
    }

    @Test
    fun formulString0() {
        val tmp = "a+b"
        assertEquals("ab@".plus(tmp), createTenableFunctionString(tmp))
    }

    @Test
    fun formulString1() {
        assertEquals("abc@a+b+c", createTenableFunctionString("a+b+c"))
    }

    @Test
    fun formulString2() {
        assertEquals("abc@(a+b)*c", createTenableFunctionString("(a+b)*c"))
    }

    @Test
    fun formulString3() {
        assertEquals("abcd@(c+a)*(b+d)", createTenableFunctionString("(c+a)*(b+d)"))
    }

    @Test
    fun formulString4() {
        assertEquals("abcde@d+c+e*(a-b)", createTenableFunctionString("d+c+e*(a-b)"))
    }

    @Test
    fun formulString5() {
        assertEquals("abcd@a+sin(c)+b*d-a", createTenableFunctionString("a+sin(c)+b*d-a"))
    }

    @Test
    fun formulString6() {
        assertEquals("a@sin(a)*pi", createTenableFunctionString("sin(a)*pi"))
    }

    @Test
    fun formulString7() {
        assertEquals("abc@ln(a)+sin(b)+c", createTenableFunctionString("ln(a)+sin(b)+c"))
    }

    @Test
    fun formulString8() {
        assertEquals("@", createTenableFunctionString(""))
    }

    // Parametru ievietošana formulā
    @Test
    fun insertParametrsInFormula0() {
        val formul = "@1+2"
        val param = "5"
        assertEquals("1+2", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula01() {
        val formul = "@1+2"
        val param = ""
        assertEquals("1+2", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula() {
        val formul = "a@a"
        val param = "5"
        assertEquals("5", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula00() {
        val formul = "a@a"
        val param = ""
        assertEquals("a", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula02() {
        val formul = "ab@(a+b)"
        val param = "1,2"
        assertEquals("(1+2)", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula1() {
        val formul = "ab@a+b"
        val param = "1,2"
        assertEquals("1+2", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula2() {
        val formul = "ab@a+b"
        val param = ",2"
        assertEquals("a+2", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula3() {
        val formul = "ab@a+b"
        val param = "1,"
        assertEquals("1+b", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula4() {
        val formul = "ab@a+b"
        val param = ","
        assertEquals("a+b", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula41() {
        val formul = "ab@a+b"
        val param = ""
        assertEquals("a+b", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula42() {
        val formul = "ab@a+b"
        val param = " "
        assertEquals("a+b", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula5() {
        val formul = "ab@b+a"
        val param = "1,2.5"
        assertEquals("2.5+1", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula6() {
        val formul = "abcd@a+sin(c)+b*d-a"
        val param = "1,2,3,4"
        assertEquals("1+sin(3)+2*4-1", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula61() {
        val formul = "abcd@a+sin(c)+b*d-a"
        val param = "11,22,,4"
        assertEquals("11+sin(c)+22*4-11", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula62() {
        val formul = "abcd@a+sin(c)+b*d-a"
        val param = "1,2,3"
        assertEquals("1+sin(3)+2*d-1", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula63() {
        val formul = "abcd@a+sin(c)+b*d-a"
        val param = "1,2,3,4,5"
        assertEquals("1+sin(3)+2*4-1", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula64() {
        val formul = "abcd@a+sin(c)+b*d-a"
        val param = ", 2,,4"
        assertEquals("a+sin(c)+2*4-a", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula7() {
        val formul = "a@sin(a)*pi"
        val param = "1,2"
        assertEquals("sin(1)*pi", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula8() {
        val formul = "a@sin(a)*pi"
        val param = "1"
        assertEquals("sin(1)*pi", createAccountFormula(param, formul))
    }

    @Test
    fun insertParametrsInFormula9() {
        val formul = "abc@sin(a)*pi+cos(ln(b)*c)"
        val param = "sqrt(9),20,tg(90)"
        assertEquals("sin(sqrt(9))*pi+cos(ln(20)*tg(90))", createAccountFormula(param, formul))
    }

    // Parametru sākuma pozīcija
    @Test
    fun findParametrsBeginPozition() {
        val mString = "1,2"
        val curPoz = 3
        assertEquals(0, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition0() {
        val mString = "1,2, 3"
        val curPoz = 0
        assertEquals(0, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition1() {
        val mString = "1,2.5,"
        val curPoz = 3
        assertEquals(0, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition11() {
        val mString = "+ 1,2.5,+"
        val curPoz = 4
        assertEquals(2, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition12() {
        val mString = "+ 1,  2.5,+"
        val curPoz = 4
        assertEquals(2, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition2() {
        val mString = ",,,3"
        val curPoz = 2
        assertEquals(0, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition21() {
        val mString = "+,,,3 +"
        val curPoz = 3
        assertEquals(1, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition3() {
        val mString = "25+cos(90)-1,2,3/25+4"
        val curPoz = 16
        assertEquals(11, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition31() {
        val mString = "25+cos(90)-1,2,3/25+4"
        val curPoz = 11
        assertEquals(11, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition32() {
        val mString = "25+cos(90)-1, 2,3/25+4"
        val curPoz = 13
        assertEquals(11, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition4() {
        val mString = "10+11+4+15"
        val curPoz = 3
        assertEquals(3, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition41() {
        val mString = "10+11+4+15"
        val curPoz = 1
        assertEquals(0, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition42() {
        val mString = "10-11*4+15"
        val curPoz = 5
        assertEquals(3, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition43() {
        val mString = "10+"
        val curPoz = 2
        assertEquals(0, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition46() {
        val mString = "10+"
        val curPoz = 3
        assertEquals(3, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition44() {
        val mString = ""
        val curPoz = 0
        assertEquals(0, findParametrsBegin(curPoz, mString))
    }

    @Test
    fun findParametrsBeginPozition45() {
        val mString = "+11*4+15"
        val curPoz = 0
        assertEquals(0, findParametrsBegin(curPoz, mString))
    }

    // Parametru beigu pozīcija
    @Test
    fun findParametrsEndPozition() {
        val mString = "+"
        val curPoz = 1
        assertEquals(1, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition0() {
        val mString = "+"
        val curPoz = 0
        assertEquals(0, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition01() {
        val mString = ""
        val curPoz = 0
        assertEquals(0, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition1() {
        val mString = "1,2.5,"
        val curPoz = 1
        assertEquals(6, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition11() {
        val mString = "+ 1,2.5,+"
        val curPoz = 4
        assertEquals(8, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition2() {
        val mString = ",,,3"
        val curPoz = 2
        assertEquals(4, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition21() {
        val mString = "+,,,3 +"
        val curPoz = 3
        assertEquals(5, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition3() {
        val mString = "25+cos(90)-1,2,3/25+4"
        val curPoz = 15
        assertEquals(16, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition31() {
        val mString = "25+cos(90)-1,2,3/25+4"
        val curPoz = 11
        assertEquals(16, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition32() {
        val mString = "25+cos(90)-1,2,3/25+4"
        val curPoz = 12
        assertEquals(16, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition33() {
        val mString = "cos(90)-1,2,sin(3 )/25+4"
        val curPoz = 8
        assertEquals(19, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition34() {
        val mString = "cos(90)-1,2,sin(3) /25+4"
        val curPoz = 12
        assertEquals(18, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition4() {
        val mString = "10+11+4+15"
        val curPoz = 3
        assertEquals(5, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition41() {
        val mString = "10+11+4+15"
        val curPoz = 4
        assertEquals(5, findParametrsEnd(curPoz, mString))
    }

    @Test
    fun findParametrsEndPozition42() {
        val mString = "10+11+4+15"
        val curPoz = 5
        assertEquals(5, findParametrsEnd(curPoz, mString))
    }

    // No ekrana rindas un dotās kursora pozīcijas atgriež parametru String
    @Test
    fun findParametrsInString0() {
        val mString = ""
        val curPoz = 0
        assertEquals("", findAndCreateParametrString(curPoz, mString))
    }

    @Test
    fun findParametrsInString1() {
        val mString = "25+cos(90)-1,2,3/25+4"
        val curPoz = 12
        assertEquals("1,2,3", findAndCreateParametrString(curPoz, mString))
    }

    @Test
    fun findParametrsInString2() {
        val mString = "10+11+4+15"
        val curPoz = 3
        assertEquals("11", findAndCreateParametrString(curPoz, mString))
    }

    @Test
    fun findParametrsInString42() {
        val mString = "10+11+4+15"
        val curPoz = 5
        assertEquals("11", findAndCreateParametrString(curPoz, mString))
    }

    @Test
    fun findParametrsInString() {
        val mString = "1,2, 3"
        val curPoz = 0
        assertEquals("1,2,3", findAndCreateParametrString(curPoz, mString))
    }
}
