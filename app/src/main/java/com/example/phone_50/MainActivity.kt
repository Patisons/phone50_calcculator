package com.example.phone_50

import android.app.PendingIntent.getActivity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.MotionEvent
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Math.round
import java.util.*
import kotlin.concurrent.timerTask
import android.widget.*
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT
import android.content.Context.INPUT_METHOD_SERVICE
import android.support.v4.app.SupportActivity
import android.support.v4.app.SupportActivity.ExtraData
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.view.inputmethod.InputMethodManager


class MainActivity : AppCompatActivity() {

    // TextView used to display the input and output
    lateinit var txtInput: TextView
    // Represent whether the lastly pressed key is numeric or not
    var lastNumeric: Boolean = false
    // Represent that current state is in error or not
    var stateError: Boolean = false
    // If true, do not allow to add another DOT
    var lastDot: Boolean = false
    // Open brackets count
    var countBracket = 0
    // vai drīkst ievadīt ","
    var allowComma = false
    // vai rāda klavieri
    var keyboardShow = true
    var pressStart = false

    // myHistory Obj
    var myHistCont = cHystory("")

    // Vai ir nospiesta poga
    var btnPresed = false
    // Vai rāda saistītos elementus
    var linkedShow = false
    // Kura poga nospiesta
    var elemTagFirst = "()"
    var elemTagSecond = "()"
    var myAction = "()"
    var actX = 0
    var actY = 0

    // mainīgie pogu un txtInput parametriem
    val elemTagId = mutableMapOf<String, Int>()
    val elemIdxTag = mutableMapOf<Int, String>()
    var elemCordList = mutableListOf<IntArray>()
    // Clear pogas karodziņš, lai atšķirtu ilgumu
    var clearFlag = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        txtInput = findViewById(R.id.txtInput)
        txtInput.requestFocus()

        // saveParametrs() palaižas, kad bilde parādījusies
        var mV = txtView
        Timer().schedule(timerTask {
            saveParametrs(mV)
        }, 100)
    }

    fun saveParametrs(view: View) {
        this.runOnUiThread {
            if (view.visibility == View.VISIBLE) {
                val vCount = myAppLayout.childCount
                for (i in 0..vCount - 1) {
                    val mElem = myAppLayout.getChildAt(i)
                    elemTagId.put(mElem.tag.toString(), mElem.id)
                    val indx = elemCordList.count()
                    elemIdxTag.put(indx, mElem.tag.toString())
                    val mCord = IntArray(4)
                    val locat = IntArray(2)
                    mElem.getLocationOnScreen(locat)
                    mCord[0] = locat[0]
                    mCord[1] = mCord[0] + mElem.width
                    mCord[2] = locat[1]
                    mCord[3] = mCord[2] + mElem.height
                    elemCordList.add(mCord)
                }
                getEditText(view).setSelection(0)
                Timer().schedule(timerTask { mKeyboardHide(view) }, 300)
            }
        }
        clearFlag = false
        createButtonList()
    }

    fun keyboardVisiblControl(view: View = myAppLayout) {
//        this.runOnUiThread {
//            if (pressStart) {
//                Timer().schedule(timerTask { keyboardVisiblControl() }, 400)
//            }
//        }
        this.runOnUiThread {
            txtInput.text = "pres=$pressStart  show=$keyboardShow"
//             else {
            if (keyboardShow) {
                mKeyboardHide(view)
            } else {
                if (view.requestFocus()) {
                    mKeyboardShow(view)
                }
            }
        }
    }

    fun mKeyboardShow(view: View) {
        val imm =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        keyboardShow = true
    }

    fun mKeyboardHide(view: View) {
        val imm: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(
            view.getWindowToken(),
            InputMethodManager.HIDE_IMPLICIT_ONLY
        )
        keyboardShow = false
    }

    fun showLinkedButton(mTag: String) {
        if (!btnPresed) return
        this.runOnUiThread(Runnable {
            val linkedButton = checkLinkedButtonList(mTag)
            linkedButton.forEach {
                setElemDrawable(getViewFromTag(it), "allowed")
            }
            linkedShow = true
        })
    }

    fun clearLinkedBorder(mTag: String) {
        // dzēš linked rāmjus
        val linkedButton = checkLinkedButtonList(mTag)
        linkedButton.forEach {
            setElemDrawable(getViewFromTag(it), "UP")
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent?): Boolean {
        actX = round(event!!.rawX)
        actY = round(event.rawY)
        val elemTag = getTagFromXY(actX, actY)
        val myElem = getViewFromTag(elemTag)
        var elemType = ""
        when (event.action) {
            // rāmis pogām un txt laukiem, kas nospiests un saliek atļautos
            MotionEvent.ACTION_DOWN -> {
                myAction = "DOWN"
                if (!btnPresed) {
                    elemTagFirst = "()"
                    elemTagSecond = "()"
                    setElemDrawable(myElem, myAction)
                    btnPresed = true
                    elemTagFirst = elemTag
                    if (elemTagFirst.equals("btnBack")) {
                        val startPoz = txtInput.selectionStart
                        val endPoz = txtInput.selectionEnd
                        Timer().schedule(timerTask {
                            onClearAll(startPoz, endPoz)
                        }, 400)
                    }
                }
            }

            MotionEvent.ACTION_UP -> {
                // todo nodzēšas visi iezīmētie, izņemot txtInput....
                myAction = "UP"
                // dzēš linked rāmjus
                clearLinkedBorder(elemTagFirst)
                setElemDrawable(getViewFromTag(elemTagFirst), myAction)
                if (linkedShow && !elemTagFirst.equals(elemTag)) {
                    linkedShow = false
                    // darbība, ja pogas saistītas
                    if (linkedButtonEquel(elemTagFirst, elemTag)) {
                        val mTag = elemTagFirst.plus(" " + elemTag)
                        when (mTag) {
                            "inpTxtView vieTxtView" -> onEqual()
                            "inpTxtView btnFunc1", "inpTxtView btnFunc2",
                            "inpTxtView btnFunc3", "inpTxtView btnFunc4",
                            "inpTxtView btnFunc5" -> setFunctionValue(mTag)
                            "btnFunc1 inpTxtView", "btnFunc2 inpTxtView",
                            "btnFunc3 inpTxtView", "btnFunc4 inpTxtView",
                            "btnFunc5 inpTxtView" -> getFunctionValue(mTag)
                            "btnFunc1 btnBack", "btnFunc2 btnBack",
                            "btnFunc3 btnBack", "btnFunc4 btnBack",
                            "btnFunc5 btnBack" -> clearFunctionString(mTag)
                            "vieTxtView inpTxtView" -> historyView()
                        }
                    }

                } else {
                    linkedShow = false
                    // izvada pogas otro nozīmi
                    if (!elemTagFirst.equals(elemTagSecond) && !elemTagSecond.equals("()")) {
                        inputLetter(myElem, findSecondSymbol(elemTagFirst))
                        setElemDrawable(getViewFromTag(elemTagFirst), myAction)
                    }
                    elemType = elemTagFirst.substring(0, 3)
                    val mElemType = elemTagFirst.subSequence(3, 6).toString()
                    if (elemType.equals("btn") && (elemTagFirst.equals(elemTagSecond) || elemTagSecond.equals(
                            "()"
                        ))
                    ) {
                        setElemDrawable(myElem, myAction)
                        // todo act,y vai poga actbtntag, ja jā tad nospiesta un uz sub cip, vai zīme
                        when (mElemType) {
                            "Dig" -> onDigit(myElem)
                            "Dot" -> onDecimalPoint(myElem)
                            "Com" -> onComma()
                            "Ope" -> onOperator(myElem)
                            "Vie" -> onEqual(myElem)
                            "Bac" -> onClearLetter()
                            "Iek" -> onBrackets(myElem)
                            "Fun" -> onFunction(myElem)
                            "Fux" -> onMyFunc(myElem)
                            "Lro" -> rewCursorPozition()
                            "Rro" -> forCursorPozition()
                        }
                    }
                    if (!elemTagFirst.equals("()")) {
                        setElemDrawable(myElem, myAction)
                    }

                }
                btnPresed = false
            }

            MotionEvent.ACTION_MOVE -> {
                // todo kaut kur bauda vai nav uz atļautā
                myAction = "MOVE"
                // iezīmē vai nē taustiņu, ja uz to atgriežas un nav otrā nozīme
                if (elemTagFirst.equals(elemTag)) {
                    setElemDrawable(myElem, "DOWN")
                }
                // todo vajag karodziņu, kā back, ja atlaiž,lai zaļo neliek
                // iekrāso saistītos
                if (elemTagFirst.equals(elemTag) && !linkedShow && checkLinkedButtonCount(
                        elemTagFirst
                    )
                ) {
                    Timer().schedule(timerTask {
                        showLinkedButton(elemTag)
                    }, 600)
                    linkedShow = true
                }
                if (!elemTagFirst.equals(elemTag) && elemTagSecond.equals("()")) {
                    elemTagSecond = elemTag
                }
                if (linkedShow) {
                    if (!elemTagFirst.equals(elemTag)) {
                        setElemDrawable(getViewFromTag(elemTagFirst), myAction)
                    }
                    if (!elemTagFirst.equals(elemTagSecond) && !elemTagSecond.equals(elemTag)
                        && linkedButtonEquel(elemTagFirst, elemTagSecond)
                    ) {
                        setElemDrawable(getViewFromTag(elemTagSecond), "allowed")
                    }
                    if (!elemTagFirst.equals(elemTag) && linkedButtonEquel(elemTagFirst, elemTag)) {
                        setElemDrawable(getViewFromTag(elemTag), "DOWN")
                        elemTagSecond = elemTag
                    }
                } else {
                    if (!elemTagFirst.equals(elemTag) && !findSecondSymbol(elemTagFirst).equals("")) {
                        setElemDrawable(getViewFromTag(elemTagFirst), myAction)
                    }
                    if (!elemTagSecond.equals(elemTag) && !elemTagSecond.equals("()")) {
                        setElemDrawable(getViewFromTag(elemTagSecond), "UP")
                        if (elemTagFirst.equals(elemTag)) {
                            elemTagSecond = "()"
                        }
                    }
                    if (btnPresed) {
                        elemTagSecond = elemTag
                    }
                }
            }
        }
        return true
    }

    fun setElemDrawable(elemView: View, mAction: String) {
        val elemTag = getTagFromView(elemView)
        val elemType = elemTag.substring(0, 3)
        when (mAction) {
            "DOWN" -> {
                when (elemType) {
                    "btn" -> elemView.setBackgroundResource(R.drawable.button_presed)
                    "vie" -> elemView.setBackgroundResource(R.drawable.view_select)
                    "inp" -> {
                        pressStart = true
                        elemView.setBackgroundResource(R.drawable.input_select)
                        txtInput.requestFocus()
//                        if (elemView.requestFocus()) {
//                            val imm =
//                                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//                            imm.showSoftInput(elemView, InputMethodManager.SHOW_IMPLICIT)
//                        }

//                        Timer().schedule(timerTask { keyboardVisiblControl() }, 200)
                    }
                }
            }
            "MOVE" -> {
                when (elemType) {
                    "btn" -> elemView.setBackgroundResource(R.drawable.button_moved)
                    "vie" -> elemView.setBackgroundResource(R.drawable.view_moved)
                    "inp" -> {
                        elemView.setBackgroundResource(R.drawable.input_moved)
                    }
                }
            }
            "UP" -> {
                when (elemType) {
                    "btn" -> elemView.setBackgroundResource(R.drawable.button_up)
                    "vie" -> elemView.setBackgroundResource(R.drawable.view_normal)
                    "inp" -> {
                        pressStart = false
                        keyboardVisiblControl()
                        elemView.setBackgroundResource(R.drawable.input_normal)
                    }
                }
            }
            "allowed" -> {
                when (elemType) {
                    "btn" -> elemView.setBackgroundResource(R.drawable.button_allowed)
                    "vie" -> elemView.setBackgroundResource(R.drawable.view_allowed)
                    "inp" -> elemView.setBackgroundResource(R.drawable.input_allowed)
                }
            }
        }
    }

    /**
     * Insert letter or string to textInput area in cursor pozition
     **/
    fun inputLetter(view: View = View(this), myString: String) {
        val startPoz = this.txtInput.selectionStart
        val endPoz = this.txtInput.selectionEnd
        var txtTmp = this.txtInput.text
        val stringBegin = txtTmp.substring(0, startPoz)
        val stringEnd = txtTmp.substring(endPoz, txtTmp.length)
        historySave("Add symbol to", txtTmp.toString())
        txtTmp = stringBegin + myString + stringEnd
        txtInput.text = txtTmp
        getEditText(view).setSelection(startPoz + myString.length)
        setInputControlFlags(view)
    }

    fun setInputControlFlags(view: View) {
        var startPoz = this.txtInput.selectionStart
        if (startPoz == 0) {
            lastNumeric = false
            stateError = false
            lastDot = false
            countBracket = 0
            allowComma = true
            return
        }
        val txtTmp = this.txtInput.text
        var charTmp = txtTmp.substring(startPoz - 1, startPoz)
        when (charTmp) {
            ")" -> {
                allowComma = true
            }
            "." -> {
                lastNumeric = false
                lastDot = true
                allowComma = false
            }
            "," -> {
                lastNumeric = false
                stateError = false
                lastDot = false
                allowComma = true
            }
            in "a".."z" -> {
                lastNumeric = true
                stateError = false
                lastDot = false
                allowComma = true
            }
            in "0".."9" -> {
                lastNumeric = true
                allowComma = true
                lastDot = false
                // pārbaude uz "."
                var myPoz = startPoz
                while (myPoz > 0) {
                    charTmp = txtTmp.substring(myPoz - 1, myPoz)
                    when (charTmp) {
                        in "0".."9" -> myPoz--
                        "." -> {
                            myPoz = 0
                            lastDot = true
                        }
                        else -> myPoz = 0
                    }
                }
            }
            "+", "-", "*", "/" -> {
                lastNumeric = false
                lastDot = false
                allowComma = true
            }
        }
        // iekavu parrēķināšana
        var openBracket = 0
        var closeBracket = 0
        while (startPoz > 0) {
            charTmp = txtTmp.substring(startPoz - 1, startPoz)
            when (charTmp) {
                "(" -> openBracket++
                ")" -> closeBracket++
            }
            startPoz--
        }
        countBracket = openBracket - closeBracket
    }

    fun onDigit(view: View) {
        setInputControlFlags(view)
        if (stateError) {
            // If current state is Error, replace the error message
            inputLetter(view, "Digit Input Error")
            // todo šito stateError vajadzētu novākt, laikam
            stateError = false
        } else {
            // If not, already there is a valid expression so append to it
            inputLetter(view, (view as Button).text.toString())
        }
        // Set the flag
        // todo kad nostrādā pirmā, var aktivizēt nākošo
    }

    /**
     * Append . to the TextView
     */
    fun onDecimalPoint(view: View) {
        setInputControlFlags(view)
        if (lastNumeric && !stateError && !lastDot) {
            inputLetter(view, ".")
        }
    }

    /**
     * Append , to the TextView
     */
    fun onComma(view: View = View(this)) {
        setInputControlFlags(view)
        if (!stateError && allowComma) {
            inputLetter(view, ",")
        }
    }

    /**
     * Append +,-,*,/ operators to the TextView
     */
    fun onOperator(view: View) {
        setInputControlFlags(view)
        val symb = (view as Button).text.toString()
        if (lastNumeric && !stateError) {
            inputLetter(view, symb)
        } else if (!lastNumeric && !stateError && symb.equals("-")) {
            inputLetter(view, symb)
        }
    }

    /**
     * Clear the TextView
     */
    fun onClearLetter(view: View = txtInput) {
        val mText = txtInput.text
        val startPoz = txtInput.selectionStart
        val endPoz = txtInput.selectionEnd
        if (!clearFlag) {
            if (mText.length > 0) {
                if (startPoz == endPoz && endPoz == mText.length) {
                    val newString = mText.substring(startIndex = 0, endIndex = endPoz - 1)
                    txtInput.text = newString
                    historySave("Clear last symbol", mText.toString())
                    getEditText(view).setSelection(txtInput.length())
                } else if (startPoz == endPoz && endPoz < mText.length && endPoz > 0) {
                    val newText = mText.subSequence(0, startPoz - 1).toString()
                    val newText2 = mText.subSequence(endPoz, mText.length).toString()
                    historySave("Clear symbol from", mText.toString())
                    txtInput.text = newText.plus(newText2)
                    getEditText(view).setSelection(if (startPoz > 0) startPoz - 1 else 0)
                }
            }
            stateError = false
        }
        clearFlag = false
    }

    fun onClearAll(startS: Int, endS: Int) {
        this.runOnUiThread(Runnable {
            val view = txtInput
            val btnPass = elemTagFirst.equals(elemTagSecond)
            val text = txtInput.text
            var myHistoryString: String
            if (btnPass && btnPresed) {
                if (startS.equals(endS)) {
                    myHistoryString = text.substring(0, endS)
                    txtInput.text = text.subSequence(endS, text.length)
                } else {
                    val newText = text.subSequence(1, startS).toString()
                    val newText2 = text.subSequence(endS, text.length).toString()
                    myHistoryString = text.substring(startS, endS)
                    txtInput.text = newText.plus(newText2)
                }
                historySave("Clear", myHistoryString)
                stateError = false
                clearFlag = true
                getEditText(view).setSelection(0)
            }
        })
    }

    /**
     * Calculate the output using Exp4j
     */
    fun onEqual(view: View = txtInput) {
        // todo pēc rezultāta izvades, kursoru nolikt rezultāta beigās
        // If the current state is error, nothing to do.
        // If the last input is a number only, solution can be found.
        if (lastNumeric && !stateError) {
            // Read the expression
            val txt = txtInput.text.toString()
            historySave("Calculate", txt + "=")
            val result = calculationOfExpression(txt)
            if (result.equals("error")) {
                historySave("Parssing error!", "")
                stateError = true
            } else {
                txtInput.text = result
            }
            getEditText(view).setSelection(txtInput.length())
        }
    }

    fun getEditText(view: View): EditText {
        val mId = getIdFromTag("inpTxtView")
        return myAppLayout.findViewById<EditText>(mId)
    }

    // Append the brackets ( )
    fun onBrackets(view: View) {
        setInputControlFlags(view)
        if (!lastNumeric && !lastDot) {
            inputLetter(view, "(")
            countBracket++
        }
        if (lastNumeric && countBracket > 0) {
            inputLetter(view, ")")
            countBracket--
        }
    }

    fun onMyFunc(view: View) {
        // te zilās f-jas apstrāde
        setInputControlFlags(view)
        if (stateError || lastNumeric || lastDot) return
        val popupMenu = PopupMenu(this, view)
        val inflater = popupMenu.menuInflater
        inflater.inflate(R.menu.header_menu, popupMenu.menu)
        popupMenu.show()
        popupMenu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.id_abs -> inputLetter(view, "abs()")
                R.id.id_ceil -> inputLetter(view, "ceil()")
                R.id.id_floor -> inputLetter(view, "floor()")
                R.id.id_round -> inputLetter(view, "round(,)")
                R.id.id_sign -> inputLetter(view, "sign()")
                R.id.id_div -> inputLetter(view, "div(,)")
                R.id.id_mod -> inputLetter(view, "mod(,)")
                R.id.id_pow -> inputLetter(view, "pow(,)")
                R.id.id_sqrt -> inputLetter(view, "sqrt()")
                R.id.id_exp -> inputLetter(view, "exp()")
                R.id.id_ln -> inputLetter(view, "ln()")
                R.id.id_lg -> inputLetter(view, "lg()")
                R.id.id_log -> inputLetter(view, "log(,)")
                R.id.id_pi -> inputLetter(view, "pi")
                R.id.id_sin -> inputLetter(view, "sin()")
                R.id.id_asin -> inputLetter(view, "asin()")
                R.id.id_cos -> inputLetter(view, "cos()")
                R.id.id_acos -> inputLetter(view, "acos()")
                R.id.id_tan -> inputLetter(view, "tan()")
                R.id.id_atan -> inputLetter(view, "atan()")
                //   ...........
                // todo ja viens argumets un pēdējā ir aizverošā iekava,
                //  tad -1 pozīcija, lai nav pie katra
            }
            rewCursorPozition(view)
            true
        }
    }

    // cursors 1 pozīcija mīnus
    fun rewCursorPozition(view: View = txtInput) {
        val startPoz = txtInput.selectionStart
        getEditText(view).setSelection(if (startPoz > 0) startPoz - 1 else 0)
    }

    // cursors 1 pozīcija plus
    fun forCursorPozition(view: View = txtInput) {
        val startPoz = txtInput.selectionStart
        getEditText(view).setSelection(if (startPoz < txtInput.length()) startPoz + 1 else startPoz)
    }

    fun saveFunction(funcNameNumber: String, mFunc: String = "x@x") {
        var funName = "btnFunc"
        funName += funcNameNumber
        val myPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        val myEditor = myPreferences.edit()
        myEditor.putString(funName, mFunc)
        myEditor.apply()
        myEditor.commit()
    }

    fun loadFunction(funcNameNumber: String): String {
        var funName = "btnFunc"
        funName += funcNameNumber
        val myPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        val mFunc = myPreferences.getString(funName, "x@x").toString()
        return mFunc
    }

    fun getTagFromId(mId: Int): String {
        try {
            val myElem = myAppLayout.findViewById<View>(mId)
            return myElem.tag.toString()
        } catch (e: Exception) {
            return "()"
        }
    }

    fun getTagFromView(mView: View): String {
        try {
            return mView.tag.toString()
        } catch (e: Exception) {
            return "()"
        }
    }

    fun getViewFromTag(mTag: String): View {
        val mId = getIdFromTag(mTag)
        return getViewFromId(mId)
    }

    fun getViewFromId(mId: Int): View {
        var myElem: View = View(this)
        try {
            myElem = myAppLayout.findViewById(mId)
        } catch (e: Exception) {
        }
        return myElem
    }

    fun getIdFromTag(mTag: String): Int {
        var mId: Int
        try {
            mId = elemTagId[mTag]!!.toInt()
        } catch (e: Exception) {
            mId = 0
        }
        return mId
    }

    fun getTagFromXY(x: Int, y: Int): String {
        var tag: String = "()"
        for (i in 0..elemCordList.count() - 1) {
            val elem = elemCordList[i]
            if ((elem[0] <= x) and (elem[1] >= x) and (elem[2] <= y) and (elem[3] >= y)) {
                tag = elemIdxTag.get(i).toString()
                break
            }
        }
        return tag
    }

    // txtInput uz f-ju
    fun setFunctionValue(mDoubleTag: String) {
        val fNum = returnFunctionButtonNumber(mDoubleTag)
        if (fNum < 0) return
        var tmp = txtInput.text.toString()
        tmp = createTenableFunctionString(tmp)
        saveFunction(fNum.toString(), tmp)
    }

    // f-jas dzēšana
    fun clearFunctionString(mDoubleTag: String) {
        val fNum = returnFunctionButtonNumber(mDoubleTag)
        if (fNum < 0) return
        var tmp = getClearFormula("")
        saveFunction(fNum.toString(), tmp)
    }

    // f-ja uz txtInput
    fun getFunctionValue(mDoubleTag: String) {
        val fNum = returnFunctionButtonNumber(mDoubleTag)
        if (fNum < 0) return
        var tmp = loadFunction(fNum.toString())
        historySave("Clear text", txtInput.text.toString())
        txtInput.text = getClearFormula(tmp)
    }

    // f-jas ielikšana txtInput rindā ar parametriem
    // brūno pogu apstrāde
    fun onFunction(mView: View) {
        val mTag = getTagFromView(mView)
        val fNum = returnFunctionButtonNumber(mTag)
        if (fNum < 0) return
        // nolasa txt
        var myText = txtInput.text.toString()
        // atrod parametrus beg, end
        val curPoz = txtInput.selectionStart
        val beg = findParametrsBegin(curPoz, myText)
        val end = findParametrsEnd(curPoz, myText)
        val myParametrs = findAndCreateParametrString(curPoz, myText)
        // formulā ieliek parametrus
        var myFormula = createAccountFormula(myParametrs, loadFunction(fNum.toString()))
        // formulu ieliek beg - end vietā
        myFormula = myText.substring(0, beg) + myFormula + myText.substring(end)
        historySave("Change", myText)
        txtInput.text = myFormula
    }

    // vēstures ievietošana ievades laukā
    fun historyView() {
        val tmp = myHistCont.toString()
        txtInput.text = tmp
        historySave("Undo", tmp)
    }

    fun historySave(mAction: String, mContent: String) {
        myHistCont = cHystory(mContent)
        txtView.text = mAction + " " + mContent
    }
}
