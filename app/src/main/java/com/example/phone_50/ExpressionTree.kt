package com.example.phone_50

import java.lang.Exception
import java.math.BigDecimal
import java.math.RoundingMode
import kotlin.math.*

// Bigdecimal precizitāte
val bScale = 16

fun calculationOfExpression(mString: String): String {
    var cString = mString.replace(" ", "")
    var tList = parsStringToArray(cString)
    var rez = ""
    if (tList.size > 3) {
        for (i in 1..tList.size - 2) {
            val mS = symType(tList[i])
            if (mS.equals("ope")) {
                if (tList[i - 1].equals("(") && tList[i + 1].equals(")")) {
                    tList[i - 1] = ""
                    tList[i + 1] = ""
                }
            }
        }
    }
    cString = parsArrayToString(tList)
    tList = parsStringToArray(cString)
    if (parsLeks(cString)) {
        try {
            val (rez1, rez2) = calculation(tList)
            rez = rez1.toDouble().toString()
        } catch (e: Exception) {
            rez = "error"
        }
    } else {
        rez = "error"
    }
    return rez
}

fun calculation(mList: ArrayList<String>): Pair<BigDecimal, BigDecimal> {
    var rez: BigDecimal = BigDecimal.ZERO //.setScale(bScale, RoundingMode.CEILING)
    var arg1: BigDecimal = BigDecimal.ZERO
    var arg2: BigDecimal = BigDecimal.ZERO
    var arg3: BigDecimal = BigDecimal.ZERO
    var cList = mList
    if (cList[0].equals("(")) cList = openBracket(cList)
    if (cList[0].equals("pi") && cList.size == 1) return Pair(
        Math.PI.toBigDecimal(),BigDecimal.ZERO)
    if (cList.size == 1) return Pair(cList[0].toBigDecimal().setScale(bScale,RoundingMode.CEILING), BigDecimal.ZERO)
    val fracture = findCurrentOperator(cList)
    val oper = mList[fracture]
    if (fracture > 0) {
        val leftChild = copyArrayList(cList, 0, fracture - 1)
        val (rez1, rez2) = calculation(leftChild)
        arg1 = rez1
    }
    if (cList.size - fracture > 1) {
        val rightChild = copyArrayList(cList, fracture + 1)
        val (rez1, rez2) = calculation(rightChild)
        arg2 = rez1
        arg3 = rez2
    }
    if (oper.length == 1) {
        when (oper) {
            "+" -> rez = arg1.plus(arg2)
            "-" -> rez = arg1.minus(arg2)
            "*" -> rez = arg1.times(arg2)
            "/" -> rez = arg1.div(arg2)
            "," -> return Pair(arg1, arg2)
            "^" -> {
                val rez1=arg1.toDouble()
                val rez2 = arg2.toDouble()
                rez = (Math.pow(rez1,rez2)).toBigDecimal()
            }
        }
    } else {
        rez = mFucnCalculate(oper, arg2, arg3).toBigDecimal()
    }


    return Pair(rez, rez)
}

fun mFucnCalculate(mFunc: String, bdArg1: BigDecimal, bdArg2: BigDecimal): Double {
    var rez = 0.0
    val arg1 = bdArg1.toDouble()
    val arg2 = bdArg2.toDouble()
    when (mFunc) {
        "sin" -> rez = Math.sin(arg1)
        "asin" -> rez = Math.asin(arg1)
        "cos" -> rez = Math.cos(arg1)
        "acos" -> rez = Math.acos(arg1)
        "abs" -> rez = Math.abs(arg1)
        "tan" -> rez = Math.tan(arg1)
        "atan" -> rez = Math.atan(arg1)
        "ceil" -> rez = Math.ceil(arg1)
        "floor" -> rez = Math.floor(arg1)
        "sign" -> rez = sign(arg1)
        "exp" -> rez = exp(arg1)
        "ln" -> rez = ln(arg1)
        "sqrt" -> rez = sqrt(arg1)
        "pow" -> rez = arg1.pow(arg2)
        "round" -> {
            val dec = 10.0
            val pak = dec.pow(arg2 + 1)
            val tmp = ((arg1 * pak + 5) / dec).toInt()
            rez = tmp.toDouble() / (pak / dec)
        }
        "mod" -> rez = arg1 % arg2
        "log" -> rez = log(arg1, arg2)
        "div" -> rez = ((arg1 / arg2).toInt()).toDouble()
        "lg" -> rez = log10(arg1)
    }
    return rez
}