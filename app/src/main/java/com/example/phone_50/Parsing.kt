package com.example.phone_50

import java.math.MathContext

fun findCurrentOperator(mElements: ArrayList<String>): Int {
    val mBracket = bracketsFindLevel(mElements, 1)
    var mInd = -1
    if (mBracket[0] == 0) {
        val nList = copyArrayList(mElements, mBracket[1] + 1)
        mInd = checkAllActionWithBracket(nList, mBracket[1] + 1, 0)
    } else {
        mInd = checkAllActionWithBracket(mElements, 0, 0)
    }
    return mInd
}

fun checkAllActionWithBracket(mList: ArrayList<String>, delta: Int, prior: Int = 0): Int {
    var mDel = delta
    var mPrio = prior
    var mInd = -1
    var nList = ArrayList<String>()
    val mBracket = bracketsFindLevel(mList, 1)
    if (mBracket[2] == 1) {
        nList = copyArrayList(mList, 0, mBracket[0] - 1)
        val (tmp1: Int, tmp2: Int) = actionPriorFind(nList, prior)
        if (tmp1 != -1) {
            mPrio = tmp2
            mInd = tmp1 + mDel
        }
        if (mBracket[1] != mList.size - 1) {
            mDel += mBracket[1] + 1
            nList = copyArrayList(mList, mBracket[1] + 1)
            val tmp = checkAllActionWithBracket(nList, mDel, mPrio)
            if (tmp != -1) mInd = tmp
        }
    } else {
        val (tmp1, tmp2) = actionPriorFind(mList, prior)
        if (tmp1 != -1) mInd = mDel + tmp1
    }
    return mInd
}

fun actionPriorFind(mList: ArrayList<String>, oldPrior: Int = 0): Pair<Int, Int> {
    var mInd = -1
    var mPrior = oldPrior
    var tPrior: Int
    for (i in 0..mList.size - 1) {
        val it = mList[i]
        if (symType(it).equals("fun")) {
            tPrior = parsOperPriority("fun")
        } else {
            tPrior = parsOperPriority(it)
        }
        if (mPrior < tPrior) {
            mPrior = tPrior
            mInd = i
        }
        if (tPrior > 0 && mPrior == tPrior && parsOperTrack(it)) {
            mInd = i
        }
    }
    return Pair(mInd, mPrior)
}

fun bracketsFindLevel(mElements: ArrayList<String>, fLevel: Int): Array<Int> {
    val rezIndx = arrayOf(-1, -1, 0)
    var brackLevel = 0
    var brackOpenCount = 0
    var elemLisIndex = 0
    var fixPoz = false
    while (elemLisIndex < mElements.size) {
        when (mElements[elemLisIndex]) {
            "(" -> {
                brackOpenCount++
                brackLevel++
                if (fLevel == brackLevel) {
                    rezIndx[0] = elemLisIndex
                    rezIndx[2] = brackLevel
                    fixPoz = true
                }
            }
            ")" -> {
                if (fixPoz && fLevel == brackLevel) {
                    rezIndx[1] = elemLisIndex
                    return rezIndx
                }
                brackLevel--
            }
        }
        elemLisIndex++
    }
    return rezIndx
}

fun bracketsDownLevel(mElements: ArrayList<String>): Array<Int> {
    val rezIndx = arrayOf(-1, -1, 0)
    var brackLevel = 0
    var brackOpenCount = 0
    var elemLisIndex = 0
    var fixPoz = false
    while (elemLisIndex < mElements.size) {
        when (mElements[elemLisIndex]) {
            "(" -> {
                brackOpenCount++
                brackLevel++
                if (rezIndx[2] < brackLevel) {
                    rezIndx[0] = elemLisIndex
                    rezIndx[2] = brackLevel
                    fixPoz = false
                }
            }
            ")" -> {
                if (!fixPoz && rezIndx[2] == brackLevel) {
                    rezIndx[1] = elemLisIndex
                    fixPoz = true
                }
                brackLevel--
            }
        }
        elemLisIndex++
    }
    return rezIndx
}

fun parsLeks(nString: String): Boolean {
    var rez = true
    var err = false
    var bO = 0
    var bC = 0
    var count = 0
    while (count < nString.length) {
        val sym = nString.substring(count, count + 1)
        if (symType(sym).equals("unk")) err = true
        when (sym) {
            "(" -> bO++
            ")" -> bC++
        }
        count++
    }
    if (bO != bC) err = true
    if (err) rez = false
    return rez
}

fun parsArrayToString(
    mArray: ArrayList<String>,
    beginInd: Int = 0,
    endInd: Int = mArray.size
): String {
    var mRez = ""
    val mEnd = if (endInd >= mArray.size) mArray.size - 1 else endInd
    for (i in beginInd..mEnd) {
        mRez += mArray[i]
    }
    return mRez
}

fun parsStringToArray(mString: String): ArrayList<String> {
    var elArr = arrayListOf<String>()
    var tmp: String
    mString.trim()
    var cikls = false
    var sTmp = ""
    var mIt = ""
    var mItOld = ""
    mString.forEach {
        mIt = it.toString()
        tmp = symType(mIt)
        if (mIt.equals("-") && mItOld.equals("(")) tmp = "num"
        if (tmp.equals("ope") || tmp.equals("bre") || tmp.equals("eli")) {
            if (cikls) {
                elArr.add(sTmp)
                sTmp = ""
                cikls = false
            }
            elArr.add(mIt)
        } else {
            if (cikls) {
                sTmp += mIt
            } else {
                sTmp = mIt
                cikls = true
            }
        }
        mItOld = mIt
    }
    if (cikls) elArr.add(sTmp)
    elArr = openBracket(elArr)
    return elArr
}

fun symType(s: String): String {
    var rez: String
    when (s) {
        in "0".."9", "." -> rez = "num"
        "+", "-", "*", "/", "^" -> rez = "ope"
        "(", ")" -> rez = "bre"
        "," -> rez = "eli"
        in "a".."z" -> rez = "let"
        else -> rez = "unk"
    }
    if (rez.equals("let") && s.length > 1) rez = "fun"
    return rez
}

fun parsOperPriority(mOp: String): Int {
    var mRez = 0
    val mPriority = mapOf<String, Int>(
        "," to 7,
        "+" to 6,
        "-" to 5,
        "*" to 4,
        "/" to 3,
        "^" to 2,
        "fun" to 1
    )
    if (!mPriority.containsKey(mOp)) return mRez
    mRez = mPriority.getValue(mOp)
    return mRez
}

fun parsOperTrack(oper: String): Boolean {
    //pēc operatora nosaka virzienu operāciju izpildei
    var mTrack = false
    when (oper) {
//        "^" -> mTrack = true
        "/" -> mTrack = true
    }
    return mTrack
}

fun copyArrayList(
    mList: ArrayList<String>,
    beginInd: Int,
    endInd: Int = mList.size - 1
): ArrayList<String> {
    val newList = ArrayList<String>()
    for (i in beginInd..endInd) {
        newList.add(mList[i])
    }
    return newList
}

fun openBracket(elArr: ArrayList<String>): ArrayList<String> {
// ja viss iekļauts iekavās
    var flag = true
    while (flag) {
        val mRez = bracketsFindLevel(elArr, 1)
        if (mRez[2] == 1 && mRez[0] == 0 && mRez[1] == elArr.size - 1) {
            elArr.removeAt(mRez[1])
            elArr.removeAt(0)
        } else {
            flag = false
        }
    }
    return elArr
}