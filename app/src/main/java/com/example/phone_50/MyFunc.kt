package com.example.phone_50

import java.lang.Exception

var myButtonLinks = arrayListOf(listOf(String()))
var myButtonList = mutableMapOf<String, Int>()
// Taustiņu otrā nozīme
var secondSymbol = emptyMap<String, String>()


data class cHystory(var hContent: String) {
    override fun toString(): String {
        return "$hContent"
    }
}

fun returnFunctionButtonNumber(mTag: String): Int {
    if (mTag.indexOf("Func") < 0) return -1
    val tmp = mTag.lastIndexOf("Func") + 4
    try {
        return mTag.substring(tmp, tmp + 1).toInt() - 1
    } catch (e: Exception) {
        return -1
    }
}

fun findSecondSymbol(mTag: String): String {
    if (secondSymbol.containsKey(mTag)) {
        return secondSymbol[mTag].toString()
    }
    return ""
}

fun linkedButtonEquel(mTag1: String, mTag2: String): Boolean {
    if (!checkLinkedButtonCount(mTag1)) return false
    val buttonList = checkLinkedButtonList(mTag1)
    if (buttonList.contains(mTag2)) return true
    return false
}

fun checkLinkedButtonCount(mTag: String): Boolean {
    if (myButtonList.containsKey(mTag)) {
        return true
    }
    return false
}

fun checkLinkedButtonList(mTag: String): List<String> {
    val buttonList = listOf<String>()
    if (myButtonList.containsKey(mTag)) {
        val indx = myButtonList[mTag]!!.toInt()
        return myButtonLinks[indx]
    }
    return buttonList
}

fun createButtonList() {
    myButtonLinks = arrayListOf(
        listOf("inpTxtView"),
        listOf("vieTxtView", "btnFunc1", "btnFunc2", "btnFunc3", "btnFunc4", "btnFunc5"),
        listOf("inpTxtView", "btnBack"),
        listOf("inpTxtView", "btnBack"),
        listOf("inpTxtView", "btnBack"),
        listOf("inpTxtView", "btnBack"),
        listOf("inpTxtView", "btnBack")
    )
    myButtonList = mutableMapOf(
        "vieTxtView" to 0,
        "inpTxtView" to 1,
        "btnFunc1" to 2,
        "btnFunc2" to 3,
        "btnFunc3" to 4,
        "btnFunc4" to 5,
        "btnFunc5" to 6
    )
    // Taustiņu otrā nozīme pēc tag
    secondSymbol = mapOf(
        "btnDig7" to "a",
        "btnDig8" to "b",
        "btnDig9" to "c",
        "btnDig4" to "x",
        "btnDig5" to "y",
        "btnDig6" to "z",
        "btnDig1" to "T",
        "btnDig2" to "p",
        "btnDig3" to "q",
        "btnDig0" to "F",
        "btnDot" to "<",
        "btnCom" to ">",
        "btnOpeRei" to ":",
        "btnOpeMin" to "Y",
        "btnOpeDal" to "~",
        "btnOpePlu" to ""
    )
}

fun createTenableFunctionString(myTxt: String = ""): String {
    if (myTxt.length == 1) return myTxt + "@" + myTxt
    val mTxt = myTxt.replace("\\s".toRegex(), "")
    var symbol = emptyMap<String, String>()
    for (i in 0..mTxt.length - 1) {
        var rLet = false
        var cLet = false
        var lLet = false
        if (i > 0 && mTxt[i - 1].toString() in "a".."z") rLet = true
        var tmpSym = mTxt[i].toString()
        if (tmpSym in "a".."z") cLet = true
        if (i < mTxt.length - 1 && mTxt[i + 1].toString() in "a".."z") lLet = true
        if (!rLet && cLet && !lLet) symbol = symbol.plus(tmpSym to tmpSym)
    }
    symbol = symbol.toSortedMap()
    var rezult = ""
    symbol.forEach { rezult += it.value }
    rezult += "@" + mTxt
    return rezult
}

fun getClearFormula(myFormul: String): String {
    val tmp = myFormul.lastIndexOf("@") + 1
    return myFormul.substring(tmp)
}

fun createAccountFormula(myParam: String, myFormul: String): String {
    val mParam = myParam.replace("\\s".toRegex(), "")
    var myValues = emptyMap<String, String>()
    var tmp1 = 0
    var tmp2 = 0
    var myChar = myFormul[tmp1].toString()
    while (!myChar.equals("@")) {
        myValues = myValues.plus(myChar to myChar)
        var myVal = ""
        while (tmp2 < mParam.length && !mParam[tmp2].toString().equals(",")) {
            myVal += mParam[tmp2].toString()
            tmp2++
        }
        if (tmp2 < mParam.length && mParam[tmp2].toString().equals(",")) tmp2++

        if (myVal.length > 0) myValues = myValues.plus(myChar to myVal)
        tmp1++
        myChar = myFormul[tmp1].toString()
    }
    val mFormul = myFormul.substring(tmp1 + 1)
    var nI = 0
    var newFormul = ""
    for (i in 0..mFormul.length - 1) {
        var rLet = false
        var cLet = false
        var lLet = false
        if (i > 0 && mFormul[i - 1].toString() in "a".."z") rLet = true
        var tmpSym = mFormul[i].toString()
        if (tmpSym in "a".."z") cLet = true
        if (i < mFormul.length - 1 && mFormul[i + 1].toString() in "a".."z") lLet = true
        if (!rLet && cLet && !lLet) {
            if (i > 0) {
                newFormul =
                    newFormul.substring(0, nI) + myValues.get(tmpSym)// + mFormul.substring(i + 1)
                nI = nI + myValues.get(tmpSym)!!.length
            } else {
                newFormul = myValues.get(tmpSym) + mFormul.substring(i + 1)
                nI = myValues.get(tmpSym)!!.length
            }
        } else {
            newFormul = newFormul + tmpSym
            nI++
        }
    }
    return newFormul
}

fun findAndCreateParametrString(cursorPoz: Int = 0, myTxtLine: String): String {
    // robežas abos virzienos nosaka pēc "+-*/" vai rindas beigām (vienkāršais variants)
    val begPoz = findParametrsBegin(cursorPoz, myTxtLine)
    val endPoz = findParametrsEnd(cursorPoz, myTxtLine)
    var mParam = myTxtLine.substring(begPoz, endPoz)
    mParam = mParam.replace("\\s".toRegex(), "")
    return mParam
}

fun findParametrsBegin(cursorPoz: Int = 0, myTxtLine: String): Int {
    if (myTxtLine.length == 0) return 0
    var poz = cursorPoz
    var tmp = ""
    if (myTxtLine.length == poz) {
        poz--
        tmp = myTxtLine[poz].toString()
        when (tmp) {
            "/", "*", "-", "+", "(", in "a".."z" -> return poz + 1
        }
    }
    var endFlag = false
    tmp = myTxtLine[poz].toString()
//    var tmp = myTxtLine.substring(poz-1,poz)
    while (poz > 0) {
        when (tmp) {
            "/", "*", "-", "+" -> poz--
            else -> endFlag = true
        }
        if (endFlag) break
        tmp = myTxtLine[poz].toString()
    }
    if (poz == 0) return poz
    endFlag = false
    var space = 0
    while (poz >= 0) {
        tmp = myTxtLine[poz].toString()
        when (tmp) {
            "/", "*", "-", "+" -> endFlag = true
            " " -> space++
            "," -> space = 0
        }
        if (endFlag) break
        poz--
        if (poz < 0) break
    }
    return poz + space + 1
}

fun findParametrsEnd(cursorPoz: Int = 0, myTxtLine: String): Int {
    var poz = cursorPoz
    var endFlag = false
    var space = 0
    while (poz < myTxtLine.length) {
        when (myTxtLine[poz].toString()) {
            "/", "*", "-", "+" -> endFlag = true
            " " -> space++
            "," -> space = 0
            in "0".."9", ")" -> space = 0
        }
        if (endFlag) break
        poz++
    }
    return poz - space
}

fun sumNumbers(vararg numbers: Int): Int {
    var tt = "xxx"
    var tmp =
    return numbers.sum()
}


